# Cloudron App

It contains a generic config that can be used to build ElasticSearch Cloudron app based on the official ElasticSearch docker image


Init image tag

```
Tag=cloudron-$(date +%s)
```


To build and push image to Gitlab registry:

```
$ docker build -f Dockerfile --no-cache . -t registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/elasticsearch:$Tag && \
    docker push registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/elasticsearch:$Tag
```


To install app on Cloudron:

```
$ cloudron install --no-wait --server YOUR-CLOUDRON-SERVER --location elasticsearch --image registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/elasticsearch:$Tag --token YOUR-GITLAB-TOKEN
```

Update app on Cloudron:

```
$ cloudron update --no-wait --no-backup --server YOUR-CLOUDRON-SERVER --app mosquitto  --image registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/elasticsearch:$Tag --token YOUR-GITLAB-TOKEN
```

To remove image from registry:

```
$ docker rmi -f registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/elasticsearch:$Tag
```
